//
//  BrainMaps.swift
//  googlemapsapi
//
//  Created by Bootcamp 3 on 2022-11-07.
//
import GoogleMaps
import Alamofire
import Foundation
protocol BrainDelegado {
    func mostrarpeliculas(lista: [SensorResponse])
}
struct BrainMaps{
    var delegate:BrainDelegado?
    
    func verdatos (){
        let request = AF.request("https://rald-dev.greenbeep.com/api/v1/aqi")
            .validate()
            .responseDecodable(of: [SensorResponse].self) { (response) in
              guard let datoscorrectos = response.value else { return }
                delegate?.mostrarpeliculas(lista: datoscorrectos)
                
            }

        
    }
    
}
