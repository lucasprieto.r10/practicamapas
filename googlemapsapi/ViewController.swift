//
//  ViewController.swift
//  googlemapsapi
//
//  Created by Bootcamp 3 on 2022-11-07.
//

import UIKit
import GoogleMaps

var brainmapas = BrainMaps()



class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        brainmapas.delegate = self
        brainmapas.verdatos()

        let camera = GMSCameraPosition.camera(withLatitude: -25.2819, longitude: -57.635, zoom: 10.0)
                let mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
                self.view.addSubview(mapView)

                // Creates a marker in the center of the map.
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: -25.2819, longitude: -57.635)
                marker.title = "Asuncion"
                marker.snippet = "Australia"
                marker.map = mapView
        // Do any additional setup after loading the view.
    }


}
extension ViewController: BrainDelegado {
    

    func mostrarpeliculas(lista: [SensorResponse]) {
        print(lista)
        DispatchQueue.main.async {
            let camera = GMSCameraPosition.camera(withLatitude: -25.2819, longitude: -57.635, zoom: 10.0)
            let mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
            self.view.addSubview(mapView)
            
            for i in 0..<lista.count{
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: lista[i].latitude ,longitude: lista[i].longitude)
                marker.map = mapView
                marker.title = lista[i].description
            }
        }
        //var datos = lista
        //print(datos)
    }
}

func cambiardatos (){
    
}
