//
//  DataMapas.swift
//  googlemapsapi
//
//  Created by Bootcamp 3 on 2022-11-07.
//

import Foundation
struct SensorResponse:Decodable {
    let sensor:String
    let source:String
    let description: String
    let longitude,latitude: Double
    let quality:Quality
}

struct Quality: Decodable {
    let category: String
    let index: Int
}
